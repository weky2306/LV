try:
    inp = raw_input("Unesite broj koji se nalazi u intervalu od 0.0 do 1.0")
    score = float(inp)
    if (score >= 1.0):
        print("Pokusati ponovo! Uneseni broj nalazi se izvan zadanog intervala.")
        exit()
    elif (score >= 0.9):
        print("A")
    elif (score >= 0.8):
        print("B")
    elif (score >= 0.7):
        print("C")
    elif (score >= 0.6):
        print("D")
    else:
        print("F") 
except:
    print("Greska! Unesena vrijednost nije numericka!!")
    exit()
