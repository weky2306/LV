#Zadatak prvi-Vedran Kluk
#Strpano je sve u jedan kod da ne bude previse datoteka.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

fhand = pd.read_csv('mtcars.csv')

S_mpg = pd.Series(fhand.mpg)
sort_mpg = fhand.sort(['mpg'], ascending = [1]) #Automobili s najvecom potrosnjom pomocu funkcije sort
print "\nNajvecu potrosnju imaju sljedeci auti:\n\n%s" % sort_mpg[:5]

S_cyl8 = fhand[fhand.cyl == 8]# 8 cilindara s najmanjom potrosnjom
print "\nNajmanju potrosnju imaju sljedeci auti s 8 cilindara:\n\n%s" % S_cyl8.sort(['mpg'], ascending = [1])

S_cyl6 = fhand[fhand.cyl == 6] # Srednja potrosnja-6 cilindara
print "Srednja potrosnje auta s 6 cilindara %.2f mpg." % np.average(S_cyl6.mpg)

S_cyl4 = fhand[fhand.cyl == 4] # Srednja potrosnja-4 cilindra
S_wt_cyl4 = S_cyl4[(S_cyl4.wt >= 2) & (S_cyl4.wt <= 2.2)] #Masa izmedu 2000 i 2200 lbs
print "Srednja potrosnje auta s 4 cilindara, s tezinom izmedu 2000 i 2200 lbs, %.2f mpg." % np.average(S_wt_cyl4.mpg)

S_am = fhand[fhand.am == 1] # Automatski i rucni mjennjaci
print "Rucni mjenjac ima %d auta, dok automatski mjenjac ima %d auta." % ((len(fhand.am)-len(S_am)), len(S_am))

S_hp_am = S_am[S_am.hp > 100] # Automatski mjenjaci preko 100 konja
print "U skupu je %d automobila s automatskim mjenjacem i snagom preko 100 hp." % len(S_hp_am)

wt_kg = (fhand.wt*1000)/2.2 # Pretvaranje mase automobila iz lbs u kg
print "Vrijednosti wt stupca u kg:\n\n%s" % wt_kg

