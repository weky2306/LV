#Zadatak drugi-Vedran Kluk

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

fhand = pd.read_csv('mtcars.csv')

#Dohvacanje podataka koji se traze
am_1 = fhand[fhand.am == 1]
am_0 = fhand[fhand.am == 0]

cyl_4 = fhand[fhand.cyl == 4]
cyl_6 = fhand[fhand.cyl == 6]
cyl_8 = fhand[fhand.cyl == 8]

#Spremanje podataka u liste, zbog lakoce upravljanja
V_am_1 = list(am_1.mpg)
V_am_0 = list(am_0.mpg)

V_cyl_4 = list(cyl_4.mpg)
V_cyl_6 = list(cyl_6.mpg)
V_cyl_8 = list(cyl_8.mpg)

#Rasprostiranje podataka kako bi kasniji prikaz bio jasniji na x-osi.
index_am1 = np.arange(len(V_am_1))
index_am0 = np.arange(len(V_am_0))

index_4 = np.arange(len(V_cyl_4))
index_6 = np.arange(len(V_cyl_6))
index_8 = np.arange(len(V_cyl_8))

#Prvi zadatak potrosnja automobila ovisno u broju cilindra 
#Prvi element plt.bar sluzii za postavljanje x-osi pa je tako svaki sljedeci graf pomaknut za duljinu prethodnog
fig = plt.figure()
plt.bar(index_4, V_cyl_4, color = 'chartreuse', label = 'Cetiri cilindra')
plt.bar(index_6 + len(index_4), V_cyl_6, color = 'azure', label = 'Sest cilindara')
plt.bar(index_8 + (len(index_4) + len(index_6)), V_cyl_8, color = 'coral', label = 'Osam cilindara')
plt.xlabel('Auto')
plt.ylabel('mpg')
plt.grid()
plt.legend()

#Drugi zadatak-distribucija tezine automobila 
fig = plt.figure()
plt.boxplot([cyl_4.wt, cyl_6.wt, cyl_8.wt])
plt.grid()

#Treci zadatak- rucni i automatski mjenjaci
fig = plt.figure()
plt.bar(index_am1, V_am_1, color = 'azure', label = 'Automatski')
plt.bar(index_am0 + len(index_am1), V_am_0, color = 'coral', label = 'Manual')
plt.xlabel('Auto')
plt.ylabel('mpg')
plt.grid()
plt.legend()

#Cetvrti zadatak
#Koristen scatter plot radi jednostavnosti
fig = plt.figure()
plt.scatter(am_1.hp, am_1.qsec, c = 'r', marker = 'x')
plt.scatter(am_0.hp, am_0.qsec, c = 'k', marker = 'x')
plt.xlabel('hp')
plt.ylabel('qsec')
plt.grid()
plt.show()

