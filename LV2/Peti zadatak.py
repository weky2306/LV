#Peti zadatak-Kluk

import csv #Biblioteka koja nam koristi da mozemo otvorit datoteku mtcars.csv kasnjije
import matplotlib.pyplot as plt #Biblioteka 

mpg = [] #Prazna lista vezana za potrosnju automobila
hp = []  #Prazna lista vezana za konjske snage automobila
wt = []  #Prazna lista vezana za tezinu automobila 

fname = open('mtcars.csv') #Otvaranje datoteke koja sadrzi popis 32 automobila
fhand = csv.reader(fname)  #Citanje csv datoteke koja sadrzi popis 32 automobila

for row in fhand: # Pretraga, odnosno ponavljanje ili kako se moderno kaze: iteracija xd po recima csv datoteke
  mpg.append(row[1])  #Spremi u listu sve elemente 1 stupca datotke
  hp.append(row[4])  #Spremi u listu sve elemente 4 stupca datotke
  wt.append(row[6])  #Spremi u listu sve elemente 6 stupca datotke


#Iskljucivanje prvog elementa svake liste, jer predstavljaju nepotrebnu string oznaku za stupac
x = mpg[1:]
y = hp[1:]
weight = wt[1:]

plt.scatter(x, y) #Scatter plot, prikaz grafa
for k in range(32): # k nam predstavlja tezinu koja se dodaje svakoj tocki da mozemo prikazat graf x,y
  plt.text(x[k], y[k], str(weight[k])) #Za svaku tocku dodaj i weight, tj tezinu 

plt.grid(True) #Naredba za tzv "mrezu"
plt.xlabel('mpg') #X os naziv, tj. potrosnja se prikazuje na x osi
plt.ylabel('hp') #Y os naziv, tj. konji se prikazuju na y osi
plt.show() #Prikaz podataka

fname.close()  #Zatvaranje datoteke


