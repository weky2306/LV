#Prvi zadatak-Kluk

import re #Biblioteka za regularne izraze


fname = raw_input("Unesite ime datoteke iz koje se filtriraju e-mail adrese: ") #Upisuje se ime datoteke mbox-short.txt

with open(fname) as fhand:
  for line in fhand:
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', line) #Potraga za stringom koji zadovoljava dani regularni izraz, regularni izraz s neta
    for email in emails:
      username = str(re.findall('\S+@', email)) #Izdvajanje svega prije znaka @, \S oznacava bilo koji znak, a + oznacava pojavljivanje znaka barem jednom
      print username.replace('@', '') #Zamjena @ s praznim stringom radi ispravnog ispisa

