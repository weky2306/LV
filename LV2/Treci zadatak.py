#Treci zadatak-Kluk

import numpy as np #Numpy biblioteka za statisticke funkcije
import matplotlib.pyplot as plt #Biblioteka za graficki prikaz

X = np.random.random_integers(1, 6, 100) #Nasumicnih 100 integer vrijednosti izmedu 1 i 6

plt.hist(X, bins = range(1,8), align = 'left', facecolor='green', alpha = 0.75) #Histogram s parametrima koje prima prema dokumentaciji matplotlib
plt.title("Histogram bacanja kockice") #Naziv histograma
plt.show() #Prikaz histograma
