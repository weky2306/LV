#Cetvrti zadatak-Kluk

import numpy as np #Biblioteka za numericke proracune
import matplotlib.pyplot as plt #Biblioteka za razlicite graficke prikaze

repeat = 1000 #Ponavljanje eksperimenta 1000 puta 

while(repeat != 0): #While petlja 
  X = np.random.random_integers(1, 6, 30) #30 puta se bacaju kockice, moguce dobiti od 1 do 6
  distro_N = "N(%.2f, %.2f)" % (np.average(X), np.std(X)) #average vraca vrijednost srednje vrijednosti i std vraca standardnu devijaciju 
  repeat = repeat - 1 #Smanjenje Ponavljanja eksperimenta nakon while petlje

plt.hist(X, bins = range(1,8), align = 'left', facecolor='green', alpha = 0.75) #Histogram prema dokumentaciji
plt.title(distro_N) #Naziv prema srednjoj vrijednosti i standardnoj devijaciji 
plt.show() #Prikaz histograma 



